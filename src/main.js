const { getTrafficHexColor } = require('./traffic-light');

const traffic = {
    setcolor: (word) => {
        if (word == "RED") {
            color = getTrafficHexColor(word);
            console.log(`Traffic light for word ${word} is ${color}`)
            return color
        } else if (word == "YELLOW") {
            color = getTrafficHexColor(word);
            console.log(`Traffic light for word ${word} is ${color}`)
            return color
        } else if (word == "GREEN") {
            color = getTrafficHexColor(word);
            console.log(`Traffic light for word ${word} is ${color}`)
            return color
        } else {
            console.log("Truffic light is not found")
            return undefined
        }


    }

}

console.log(traffic.setcolor("RED"));
console.log(traffic.setcolor("YELLOW"));
console.log(traffic.setcolor("GREEN"));
console.log(traffic.setcolor("blue"));

module.exports = traffic;