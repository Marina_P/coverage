const expect = require('chai').expect;
const { getTrafficHexColor } = require("../src/traffic-light");
const traffic = require("../src/main")

describe("Traffic lights", () => {
    it("Test traffic lights possibilitites", () => {
        expect(true).to.equal(true);
        expect(getTrafficHexColor("GREEN")).to.equal("#00FF00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#FFFF00");
        expect(getTrafficHexColor("RED")).to.equal("#FF0000");
        expect(getTrafficHexColor("blue")).to.equal(undefined);
    });

});

describe("Traffic light setting", () => {
    it("Try to get a hex value", () => {
        expect(true).to.equal(true);
        expect(traffic.setcolor("RED")).to.equal("#FF0000");
        expect(traffic.setcolor("YELLOW")).to.equal("#FFFF00");
        expect(traffic.setcolor("GREEN")).to.equal("#00FF00");
        expect(traffic.setcolor("blue")).to.equal(undefined);
    })
})